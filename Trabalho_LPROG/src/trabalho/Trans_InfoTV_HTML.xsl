<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:op = "http://lprog/operadora" version="1.0">
	<xsl:output method="html"/>

	<xsl:template match="/">
		<html>
			<head>
				<title>Trans_InfoTV_HTML.xsl</title>
			</head>
			<body>
				<h3>Informação Minimizada sobre o Guia de TV</h3>
				<xsl:for-each select = "operadora/canais/canal">
					<font size="5">
						<strong>Canal: <xsl:value-of select = "attribute::numero"/></strong>
						<br/>
					</font>
					<strong>Nome: </strong>
					<xsl:value-of select = "nome"/>
					<br/>
					<strong>Categoria: </strong>
					<xsl:value-of select = "categoria"/>
					<h4>Programação</h4>
					<xsl:for-each select="programas">
						<xsl:apply-templates select="programa"/>
					</xsl:for-each>
					<xsl:for-each select="programas">
						<xsl:apply-templates select="jogo"/>
					</xsl:for-each>
					<xsl:for-each select="programas">
						<xsl:apply-templates select="filme"/>
					</xsl:for-each>
				</xsl:for-each>
			</body>
		</html>
	</xsl:template>

	<xsl:template match="programa">
		<i>
			<xsl:value-of select="fotografia"/>
		</i>
		<br/>
		<br/>
		<strong>Nome: </strong>
		<xsl:value-of select="attribute::nome"/>
		<br/>
		<strong>Descrição: </strong>
		<xsl:value-of select="descricao"/>
		<br/>
		<strong>Duração: </strong>
		<xsl:value-of select="duracao"/>
		<br/>
		<strong>Episódio Atual: </strong>
		<xsl:value-of select = "episodio"/>
		<br/>
		<strong>Temporada: </strong>
		<xsl:value-of select = "temporada"/>
		<br/>
		<br/>
	</xsl:template>

	<xsl:template match="jogo">
		<i>
			<xsl:value-of select="fotografia"/>
		</i>
		<br/>
		<br/>
		<strong>Nome: </strong>
		<xsl:value-of select="attribute::nome"/>
		<br/>
		<strong>Descrição: </strong>
		<xsl:value-of select="descricao"/>
		<br/>
		<strong>Duração: </strong>
		<xsl:value-of select="duracao"/>
		<br/>
		<br/>
	</xsl:template>

	<xsl:template match="filme">
		<i>
			<xsl:value-of select="fotografia"/>
		</i>
		<br/>
		<br/>
		<strong>Nome: </strong>
		<xsl:value-of select="attribute::nome"/>
		<br/>
		<strong>Descrição: </strong>
		<xsl:value-of select="descricao"/>
		<br/>
		<strong>Duração: </strong>
		<xsl:value-of select="duracao"/>
		<br/>
		<br/>
	</xsl:template>

</xsl:stylesheet>
