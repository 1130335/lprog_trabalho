<?xml version="1.0" encoding="UTF-8"?>

<!--
	Document   : Trans_InfoCompleto_HTML.xsl
	Created on : 22 de Maio de 2015, 15:41
	Author     : Rita
	Description:
		Este documento XSLT transforma o guia de TV criado para este trabalho num ficheiro HTML que mostra a informação completa da programação completa da operadora.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:op= "http://lprog/operadora" version="1.0" >
	<xsl:output method="html"/>

	<xsl:template match="/">
		<html>
			<head>
				<title>Informação Completa da Operadora</title>
			</head>
			<body>
				<h1>Informação Completa da Operadora</h1>
				<xsl:apply-templates select="operadora/dadosgerais"/>
				<hr/>
				<xsl:apply-templates select="operadora/TV"/>
				<hr/>
				<xsl:apply-templates select="operadora/radio"/>
				<hr/>
				<xsl:apply-templates select="operadora/gravacoes"/>
				<hr/>
				<xsl:apply-templates select="operadora/videoclube"/>
			</body>
		</html>
	</xsl:template>

	<xsl:template match="operadora/dadosgerais">
		<h2>» Dados Gerais «</h2>
		<i>
			<xsl:value-of select="logotipo"/>
		</i>
		<br/>
		<br/>
		<strong>Nome: </strong>
		<xsl:value-of select="nome"/>
		<br/>
		<strong>Código: </strong>
		<xsl:value-of select="codigo"/>
		<br/>
		<strong>Data de Início: </strong>
		<xsl:value-of select="datas/dataInicio"/>
		<br/>
		<strong>Data de Atualização: </strong>
		<xsl:value-of select="datas/dataAtualizacao"/>
		<br/>
		<strong>Dias a que diz respeito a informação: </strong>
		<xsl:value-of select="dias"/>
		<br/>
	</xsl:template>

	<xsl:template match="operadora/TV">
		<h2>» Guia de Televisão «</h2>
		<xsl:for-each select = "canais/canal">
			<h3>
				Canal: <xsl:value-of select ="attribute::numero"/>
				<br/>
				Nome: <xsl:value-of select ="nome"/>
				<br/>
				Categoria: <xsl:value-of select ="categoria"/>
			</h3>
			<h4>Programação</h4>
			<xsl:for-each select="programas">
				<xsl:apply-templates select="programa"/>
			</xsl:for-each>
			<xsl:for-each select="programas">
				<xsl:apply-templates select="jogo"/>
			</xsl:for-each>
			<xsl:for-each select="programas">
				<xsl:apply-templates select="filme"/>
			</xsl:for-each>
		</xsl:for-each>
	</xsl:template>

	<xsl:template match="programa">
		<i>
			<xsl:value-of select="fotografia"/>
		</i>
		<br/>
		<br/>
		<strong>Nome: </strong>
		<xsl:value-of select="attribute::nome"/>
		<br/>
		<strong>Descrição: </strong>
		<xsl:value-of select="descricao"/>
		<br/>
		<strong>Duração: </strong>
		<xsl:value-of select="duracao"/>
		<br/>
		<strong>Total de Episódio: </strong>
		<xsl:value-of select="episodios"/>
		<br/>
		<strong>Episódio Atual: </strong>
		<xsl:value-of select = "episodio"/>
		<br/>
		<strong>Temporada: </strong>
		<xsl:value-of select = "temporada"/>
		<br/>
		<strong>Formato da Imagem: </strong>
		<xsl:value-of select = "formatoImagem"/>
		<br/>
		<strong>Idioma do Áudio: </strong>
		<xsl:value-of select = "audio"/>
		<br/>
		<br/>
	</xsl:template>

	<xsl:template match="jogo">
		<i>
			<xsl:value-of select="fotografia"/>
		</i>
		<br/>
		<br/>
		<strong>Nome: </strong>
		<xsl:value-of select="attribute::nome"/>
		<br/>
		<strong>Descrição: </strong>
		<xsl:value-of select="descricao"/>
		<br/>
		<strong>Duração: </strong>
		<xsl:value-of select="duracao"/>
		<br/>
		<strong>Formato da Imagem: </strong>
		<xsl:value-of select = "formatoImagem"/>
		<br/>
		<strong>Idioma do Áudio: </strong>
		<xsl:value-of select = "audio"/>
		<br/>
		<br/>
	</xsl:template>

	<xsl:template match="filme">
		<i>
			<xsl:value-of select="fotografia"/>
		</i>
		<br/>
		<br/>
		<strong>Nome: </strong>
		<xsl:value-of select="attribute::nome"/>
		<br/>
		<strong>Descrição: </strong>
		<xsl:value-of select="descricao"/>
		<br/>
		<strong>Duração: </strong>
		<xsl:value-of select="duracao"/>
		<br/>
		<strong>Formato da Imagem: </strong>
		<xsl:value-of select = "formatoImagem"/>
		<br/>
		<strong>Idioma do Áudio: </strong>
		<xsl:value-of select = "audio"/>
		<br/>
		<br/>
	</xsl:template>

	<xsl:template match ="operadora/radio">
		<h2>» Guia de Rádio «</h2>
		<xsl:for-each select = "canais/canal">
			<h3>
				Canal: <xsl:value-of select ="attribute::numero"/>
				<br/>
				Nome: <xsl:value-of select ="nome"/>
				<br/>
				Categoria:
				<xsl:for-each select = "categorias/categoria">
					<xsl:value-of select ="."/>...
				</xsl:for-each>
			</h3>
			<h4>Programação</h4>
			<xsl:for-each select="programas">
				<xsl:apply-templates select="programaRadio"/>
			</xsl:for-each>
		</xsl:for-each>
	</xsl:template>

	<xsl:template match = "programaRadio">
		<strong>Nome: </strong>
		<xsl:value-of select="attribute::nome"/>
		<br/>
		<strong>Duração: </strong>
		<xsl:value-of select="duracao"/>
		<br/>
		<strong>Descrição: </strong>
		<xsl:value-of select="descricao"/>
		<br/>
	</xsl:template>

	<xsl:template match ="operadora/gravacoes">
		<h3>» Gravações «</h3>
		<xsl:for-each select="episodios/episodio">
			<strong>Série: </strong>
			<xsl:value-of select="serie"/>
			<br/>
			<strong>Episódio <xsl:value-of select="attribute::numero"/>, Temporada <xsl:value-of select="temporada"/>: <xsl:value-of select="attribute::nome"/></strong>
			<br/>
			<strong>Duração: </strong>
			<xsl:value-of select="duracao"/>
			<br/>
			<strong>Data: </strong>
			<xsl:value-of select="data"/>
			<br/>
			<br/>
		</xsl:for-each>
	</xsl:template>

	<xsl:template match="operadora/videoclube">
		<h3> » Videoclube « </h3>
		<xsl:for-each select = "filmes/filme">
			<strong>Título: </strong>
			<xsl:value-of select="attribute::titulo"/>
			<br/>
			<strong>Duração: </strong>
			<xsl:value-of select="duracao"/>
			<br/>
			<strong>Ano de Estreia: </strong>
			<xsl:value-of select="ano"/>
			<br/>
			<strong>Géneros :</strong>
			<xsl:for-each select="generos/genero">
				<xsl:value-of select="."/>...
			</xsl:for-each>
			<br/>
			<br/>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>
