<?xml version="1.0" encoding="UTF-8"?>

<!--
	Document   : Trans_InfoTV.xsl
	Created on : 28 de Maio de 2015, 16:44
	Author     : Rita
	Description:
		Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:op="http://lprog/operadora" version="1.0">
	<xsl:output method="xml"/>

	<xsl:template match="/">
		<operadora xmlns="http://lprog/operadora">
			<canais>
				<xsl:apply-templates select ="operadora/TV"/>
			</canais>
		</operadora>
	</xsl:template>

	<xsl:template match="operadora/TV">
		<xsl:for-each select = "canais/canal">
			<canal>
				<xsl:attribute name = "numero">
					<xsl:value-of select = "attribute::numero"/>
				</xsl:attribute>
				<nome>
					<xsl:value-of select = "nome"/>
				</nome>
				<categoria>
					<xsl:value-of select = "categoria"/>
				</categoria>
				<programas>
					<xsl:apply-templates select = "programas"/>
				</programas>
			</canal>
		</xsl:for-each>
	</xsl:template>

	<xsl:template match = "programas">
		<xsl:for-each select = "programa">
			<programa>
				<xsl:attribute name = "nome">
					<xsl:value-of select = "attribute::nome"/>
				</xsl:attribute>
				<fotografia>
					<xsl:value-of select = "fotografia"/>
				</fotografia>
				<duracao>
					<xsl:value-of select = "duracao"/>
				</duracao>
				<descricao>
					<xsl:value-of select = "descricao"/>
				</descricao>
				<episodio>
					<xsl:value-of select = "episodio"/>
				</episodio>
				<temporada>
					<xsl:value-of select = "temporada"/>
				</temporada>
			</programa>
		</xsl:for-each>

		<xsl:for-each select = "filme">
			<filme>
				<xsl:attribute name = "nome">
					<xsl:value-of select = "attribute::nome"/>
				</xsl:attribute>
				<fotografia>
					<xsl:value-of select = "fotografia"/>
				</fotografia>
				<duracao>
					<xsl:value-of select = "duracao"/>
				</duracao>
				<descricao>
					<xsl:value-of select = "descricao"/>
				</descricao>
			</filme>
		</xsl:for-each>

		<xsl:for-each select = "jogo">
			<filme>
				<xsl:attribute name = "nome">
					<xsl:value-of select = "attribute::nome"/>
				</xsl:attribute>
				<fotografia>
					<xsl:value-of select = "fotografia"/>
				</fotografia>
				<duracao>
					<xsl:value-of select = "duracao"/>
				</duracao>
				<descricao>
					<xsl:value-of select = "descricao"/>
				</descricao>
			</filme>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>
