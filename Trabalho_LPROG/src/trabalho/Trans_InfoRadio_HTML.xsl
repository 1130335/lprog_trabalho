<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : Trans_InfoRadio_HTML.xsl
    Created on : May 29, 2015, 3:51 PM
    Author     : Aria
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:op = "http://lprog/operadora" version="1.0">
    <xsl:output method="html"/>

    <xsl:template match="/">
        <html>
            <head>
                <title>radio-HTML.xsl</title>
            </head>
            <body>
             <h2>» Guia de Rádio «</h2>
		<xsl:for-each select = "operadora/canais/canal">
			<h3>
				Canal: <xsl:value-of select ="attribute::numero"/>
				<br/>
				Nome: <xsl:value-of select ="nome"/>
				<br/>
				Categoria:
				<xsl:for-each select = "categorias/categoria">
					<xsl:value-of select ="."/>...
				</xsl:for-each>
			</h3>
			<h4>Programação</h4>
			<xsl:for-each select="programas">
				<xsl:apply-templates select="programaRadio"/>
			</xsl:for-each>
		</xsl:for-each>   
                
            </body>
        </html>
    </xsl:template>
    <xsl:template match = "programaRadio">
		<strong>Nome: </strong>
		<xsl:value-of select="attribute::nome"/>
		<br/>
		<strong>Duração: </strong>
		<xsl:value-of select="duracao"/>
		<br/>
		<strong>Descrição: </strong>
		<xsl:value-of select="descricao"/>
		<br/>
	</xsl:template>

</xsl:stylesheet>
