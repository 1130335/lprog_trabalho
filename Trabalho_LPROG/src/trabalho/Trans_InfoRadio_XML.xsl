<?xml version="1.0" encoding="UTF-8"?>

<!--
	Document   : radio.xsl
	Created on : May 28, 2015, 4:48 PM
	Author     : Aria
	Description:
		Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:op= "http://lprog/operadora" version="1.0">
	<xsl:output method="xml"/>


	<xsl:template match="/">
		<operadora xmlns="http://lprog/operadora">
			<canais>
				<xsl:apply-templates select ="operadora/radio"/>
			</canais>
		</operadora>
	</xsl:template>
	<xsl:template match="operadora/radio">
		<xsl:for-each select="canais/canal">
			<canal>
				<xsl:attribute name="numero">
					<xsl:value-of select="attribute::numero"/>
				</xsl:attribute>
				<nome>
					<xsl:value-of select="nome"/>
				</nome>
				<categorias>
					<xsl:apply-templates select="categorias"/>

				</categorias>
				<programas>
					<xsl:apply-templates select="programas"/>
				</programas>
			</canal>
		</xsl:for-each>
	</xsl:template>
	<xsl:template match = "programas">
		<xsl:for-each select = "programaRadio">
			<programaRadio>
				<xsl:attribute name = "nome">
					<xsl:value-of select = "attribute::nome"/>
				</xsl:attribute>
				<duracao>
					<xsl:value-of select = "duracao"/>
				</duracao>
				<descricao>
					<xsl:value-of select = "descricao"/>
				</descricao>
			</programaRadio>
		</xsl:for-each>
	</xsl:template>
	<xsl:template match = "categorias">
		<xsl:for-each select = "categoria">
			<categoria>

				<xsl:value-of select="."/>
			</categoria>
		</xsl:for-each>
	</xsl:template>


</xsl:stylesheet>
