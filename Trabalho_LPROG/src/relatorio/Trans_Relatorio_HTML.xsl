<?xml version="1.0" encoding="UTF-8"?>

<!--
	Document   : Trans_Relatorio_HTML.xsl
	Created on : 31 de Maio de 2015, 21:12
	Author     : Rita
	Description:
		Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:rl="http://www.dei.isep.ipp.pt/lprog" version="1.0">
	<xsl:output method="html"/>

	<!-- TODO customize transformation rules
		 syntax recommendation http://www.w3.org/TR/xslt
	-->
	<xsl:template match="/">
		<html>
			<head>
				<title>Trans_Relatorio_HTML.xsl</title>
			</head>
			<body>
				<center>
					<h1>
						Relatório de Linguagens e Programação
					</h1>
					<p>
						<xsl:apply-templates select="relatório/páginaRosto"/>
					</p>
					<hr/>
					<p>
						<h1>Indíce</h1>
						1. <xsl:value-of select = "relatório/corpo/introdução/attribute::tituloSecção"/>
						<br/>
						<br/>
						2. <xsl:value-of select = "relatório/corpo/secções/análise/attribute::tituloSecção"/>
						<br/>
						<br/>
						3. <xsl:value-of select = "relatório/corpo/secções/linguagem/attribute::tituloSecção"/>
						<br/>
						<br/>
						4. <xsl:value-of select = "relatório/corpo/secções/transformações/attribute::tituloSecção"/>
						<br/>
						<br/>
						5. <xsl:value-of select = "relatório/corpo/conclusão/attribute::tituloSecção"/>
						<br/>
						<br/>
						6. Referências
						<br/>
						<br/>
					</p>
					<hr/>
				</center>
				<p>
					<xsl:apply-templates select = "relatório/corpo/introdução"/>
					<xsl:apply-templates select = "relatório/corpo/secções/análise"/>
					<xsl:apply-templates select = "relatório/corpo/secções/linguagem"/>
					<xsl:apply-templates select = "relatório/corpo/secções/transformações"/>
					<xsl:apply-templates select = "relatório/corpo/conclusão"/>
				</p>
				<h1>
					Referências
				</h1>
				<xsl:for-each select = "relatório/corpo/referências/refWeb">
					<xsl:value-of select = "consultadoEm"/>: <xsl:value-of select = "descrição"/> = <xsl:value-of select = "URL"/>
					<br/>
					<br/>
				</xsl:for-each>
				<hr/>
				<h1>
					Anexos
				</h1>
				<br/>
				<br/>
			</body>
		</html>
	</xsl:template>

	<xsl:template match = "relatório/páginaRosto">
		<strong>
			<xsl:value-of select="disciplina/designação"/> - <xsl:value-of select = "disciplina/sigla"/> - <xsl:value-of select = "disciplina/anoCurricular"/>º Ano
		</strong>
		<br/>
		<br/>
		<xsl:for-each select = "autor">
			<xsl:value-of select = "nome"/>
			<br/>
			<xsl:value-of select = "número"/>
			<br/>
			<xsl:value-of select = "mail"/>
			<br/>
			<br/>
		</xsl:for-each>

		<xsl:value-of select = "data"/>
		<br/>
		<xsl:value-of select = "turmaPL"/>
		<br/>
		Professor <xsl:value-of select = "profPL"/>
		<br/>
	</xsl:template>

	<xsl:template match = "relatório/corpo/conclusão">
		<h1>Conclusão</h1>
		<xsl:for-each select = "bloco/parágrafo">
			<xsl:value-of select = "."/>
			<br/>
			<br/>
		</xsl:for-each>
		<hr/>
	</xsl:template>

	<xsl:template match="relatório/corpo/secções/análise">
		<h1>Análise</h1>
		<xsl:for-each select = "bloco/parágrafo">
			<xsl:value-of select = "."/>
			<br/>
			<br/>
		</xsl:for-each>
		<hr/>
	</xsl:template>

	<xsl:template match="relatório/corpo/secções/linguagem">
		<h1>Linguagem</h1>
		<xsl:value-of select = "bloco/parágrafo"/>
		<br/>
		<br/>
		<hr/>
	</xsl:template>

	<xsl:template match="relatório/corpo/secções/transformações">
		<h1>Transformações</h1>
		<xsl:value-of select = "bloco/parágrafo"/>
		<br/>
		<br/>
		<xsl:for-each select = "bloco/listaItems/item">
			<xsl:value-of select = "."/>
			<br/>
			<br/>
		</xsl:for-each>
		<xsl:value-of select = "bloco/parágrafo[2]"/>
		<br/>
		<br/>
		<hr/>
	</xsl:template>

	<xsl:template match = "relatório/corpo/introdução">
		<h1>Introdução</h1>
		<xsl:for-each select = "bloco/parágrafo">
			<xsl:value-of select = "."/>
			<br/>
			<br/>
		</xsl:for-each>
		<hr/>
	</xsl:template>
</xsl:stylesheet>
